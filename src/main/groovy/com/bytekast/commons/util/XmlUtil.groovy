package com.bytekast.commons.util

/**
 * @author Rowell Belen
 */
class XmlUtil {

  List findValueByElement(String xml, String element) {
    def root = new XmlSlurper().parseText(xml);
    return root.'**'.findAll { node -> node.name() == element }*.text()
  }

  String findValueByElementAndAttribute(String xml, String element, String attrName, String attrValue) {

    def root = new XmlSlurper().parseText(xml);
    def el = root.'**'.find { node ->
      node.name() == element && node['@' + attrName] == attrValue;
    }

    return el ? el.text() : null;
  }
}
