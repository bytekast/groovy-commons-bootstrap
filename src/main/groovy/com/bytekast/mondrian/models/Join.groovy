package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

class Join {

  def leftKey
  def rightKey
  def tables = []

  def xml = { builder ->
    builder.Join('leftKey': leftKey, 'rightKey': rightKey) {
      tables.each {
        it.xml(builder)
      }
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    leftKey = root.@leftKey
    rightKey = root.@rightKey

    tables = []
    root.Table.each {
      def obj = new Table()
      obj.load(XmlUtil.serialize(it))
      tables << obj
    }
  }
}
