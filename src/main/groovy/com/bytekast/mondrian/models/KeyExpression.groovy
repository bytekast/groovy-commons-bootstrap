package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

/**
 * @author Rowell Belen
 */
class KeyExpression {

  def sqlList = []

  def xml = { builder ->
    builder.KeyExpression() {
      sqlList.each {
        it.xml(builder)
      }
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);

    sqlList = []
    root.SQL.each {
      def obj = new SQL()
      obj.load(XmlUtil.serialize(it))
      sqlList << obj
    }
  }
}
