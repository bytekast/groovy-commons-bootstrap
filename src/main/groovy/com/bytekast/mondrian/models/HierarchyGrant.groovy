package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

/**
 * @author Rowell Belen
 */
class HierarchyGrant {

  def access
  def hierarchy
  def bottomLevel
  def topLevel
  def memberGrants = []

  def xml = { builder ->
    builder.HierarchyGrant('access': access, 'hierarchy': hierarchy, 'bottomLevel': bottomLevel, 'topLevel': topLevel) {
      memberGrants.each {
        it.xml(builder)
      }
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    access = root.@access
    hierarchy = root.@hierarchy
    bottomLevel = root.@bottomLevel
    topLevel = root.@topLevel

    memberGrants = []
    root.MemberGrant.each {
      def obj = new MemberGrant()
      obj.load(XmlUtil.serialize(it))
      memberGrants << obj
    }
  }
}
