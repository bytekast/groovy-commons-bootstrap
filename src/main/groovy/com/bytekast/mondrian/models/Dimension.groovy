package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

class Dimension {

  def name
  def type
  def foreignKey
  def hierarchies = []

  def xml = { builder ->
    builder.Dimension('name': name, 'type': type, 'foreignKey': foreignKey) {
      hierarchies.each {
        it.xml(builder)
      }
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
    type = root.@type
    foreignKey = root.@foreignKey

    hierarchies = []
    root.Hierarchy.each {
      def obj = new Hierarchy()
      obj.load(XmlUtil.serialize(it))
      hierarchies << obj
    }
  }
}
