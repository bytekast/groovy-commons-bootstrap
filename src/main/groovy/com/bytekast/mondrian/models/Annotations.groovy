package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

/**
 * @author Rowell Belen
 */
class Annotations {

  def annotations = []

  def xml = { builder ->
    builder.Annotations() {
      annotations.each {
        it.xml(builder)
      }
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);

    annotations = []
    root.Annotation.each {
      def obj = new Annotation()
      obj.load(XmlUtil.serialize(it))
      annotations << obj
    }
  }
}
