package com.bytekast.mondrian.models

class Table {

  def name

  def xml = { builder ->
    builder.Table('name': name)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
  }
}
