package com.bytekast.mondrian.models

class AggExclude {

  def name

  def xml = { builder ->
    builder.AggExclude('name': name)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
  }
}
