package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

/**
 * @author Rowell Belen
 */
class NamedSet {

  def name
  def formula

  def xml = { builder ->
    builder.NamedSet('name': name) {
      formula?.xml(builder)
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name


    if (root.Formula.size()) {
      formula = new Formula();
      formula.load(XmlUtil.serialize(root.Formula));
    }
  }
}
