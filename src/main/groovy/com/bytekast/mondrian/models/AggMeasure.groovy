package com.bytekast.mondrian.models

class AggMeasure {

  def name
  def column

  def xml = { builder ->
    builder.AggMeasure('name': name, 'column': column)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
    column = root.@column
  }
}
