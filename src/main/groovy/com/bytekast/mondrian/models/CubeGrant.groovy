package com.bytekast.mondrian.models
/**
 * @author Rowell Belen
 */
class CubeGrant {

  def access;
  def cube;

  def xml = { builder ->
    builder.CubeGrant('access': access, 'cube': cube)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    access = root.@access
    cube = root.@cube
  }
}
