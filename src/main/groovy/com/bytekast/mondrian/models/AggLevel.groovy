package com.bytekast.mondrian.models

class AggLevel {

  def name
  def column

  def xml = { builder ->
    builder.AggLevel('name': name, 'column': column)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
    column = root.@column
  }
}
