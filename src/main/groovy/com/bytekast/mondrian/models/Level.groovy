package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

class Level {

  def name
  def column
  def uniqueMembers
  def type
  def parentColumn
  def nameColumn
  def nullParentValue
  def closure
  def properties = []
  def keyExpressions = []

  def xml = { builder ->
    builder.Level('name': name, 'column': column, 'uniqueMembers': uniqueMembers, 'type': type,
        'parentColumn': parentColumn, 'nameColumn': nameColumn, 'nullParentValue': nullParentValue) {

      closure?.xml(builder)

      properties.each {
        it.xml(builder)
      }

      keyExpressions.each {
        it.xml(builder)
      }
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
    column = root.@column
    uniqueMembers = root.@uniqueMembers
    type = root.@type
    parentColumn = root.@parentColumn
    nameColumn = root.@nameColumn
    nullParentValue = root.@nullParentValue

    if (root.Closure.size()) {
      closure = new Closure()
      closure.load(XmlUtil.serialize(root.Closure))
    }

    properties = []
    root.Property.each {
      def obj = new Property()
      obj.load(XmlUtil.serialize(it))
      properties << obj
    }

    keyExpressions = []
    root.KeyExpression.each {
      def obj = new KeyExpression()
      obj.load(XmlUtil.serialize(it))
      keyExpressions << obj
    }
  }
}
