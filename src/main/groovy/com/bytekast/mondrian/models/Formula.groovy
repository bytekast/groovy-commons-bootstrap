package com.bytekast.mondrian.models

/**
 * @author Rowell Belen
 */
class Formula {

  def text;

  def xml = { builder ->
    builder.Formula(text)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    text = root.text()
  }
}
