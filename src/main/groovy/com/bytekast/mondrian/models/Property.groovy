package com.bytekast.mondrian.models

class Property {

  def name
  def column

  def xml = { builder ->
    builder.Property('name': name, 'column': column)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
    column = root.@column
  }
}
