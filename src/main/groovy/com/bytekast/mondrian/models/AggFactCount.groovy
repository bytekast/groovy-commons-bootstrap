package com.bytekast.mondrian.models

class AggFactCount {

  def column

  def xml = { builder ->
    builder.AggFactCount('column': column)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    column = root.@column
  }
}
