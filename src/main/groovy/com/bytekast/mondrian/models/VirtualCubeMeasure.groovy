package com.bytekast.mondrian.models

/**
 * @author Rowell Belen
 */
class VirtualCubeMeasure {

  def cubeName
  def name

  def xml = { builder ->
    builder.VirtualCubeMeasure('cubeName': cubeName, 'name': name)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
    cubeName = root.@cubeName
  }
}
