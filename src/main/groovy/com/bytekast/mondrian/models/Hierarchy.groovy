package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

/**
 * @author Rowell Belen
 */
class Hierarchy {

  def hasAll
  def allMemberName
  def primaryKey
  def defaultMember
  def table
  def levels = []

  def xml = { builder ->
    builder.Hierarchy('hasAll': hasAll, 'allMemberName': allMemberName, 'primaryKey': primaryKey, 'defaultMember': defaultMember) {
      table?.xml(builder)
      levels.each {
        it.xml(builder)
      }
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    hasAll = root.@hasAll
    allMemberName = root.@allMemberName
    primaryKey = root.@primaryKey
    defaultMember = root.@defaultMember

    if (root.Table.size()) {
      table = new Table()
      table.load(XmlUtil.serialize(root.Table))
    }

    levels = []
    root.Level.each {
      def obj = new Level()
      obj.load(XmlUtil.serialize(it))
      levels << obj
    }
  }
}
