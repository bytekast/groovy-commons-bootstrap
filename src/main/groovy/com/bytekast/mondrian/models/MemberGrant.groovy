package com.bytekast.mondrian.models

/**
 * @author Rowell Belen
 */
class MemberGrant {

  def access
  def member

  def xml = { builder ->
    builder.MemberGrant('access': access, 'member': member)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    access = root.@access
    member = root.@member
  }
}
