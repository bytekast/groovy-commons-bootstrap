package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

/**
 * @author Rowell Belen
 */
class Closure {

  def parentColumn
  def childColumn
  def table

  def xml = { builder ->
    builder.Closure('parentColumn': parentColumn, 'childColumn': childColumn) {
      table?.xml(builder)
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    parentColumn = root.@parentColumn
    childColumn = root.@childColumn
    if (root.Table.size()) {
      table = new Table()
      table.load(XmlUtil.serialize(root.Table))
    }
  }
}
