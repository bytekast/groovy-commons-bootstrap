package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

/**
 * @author Rowell Belen
 */
class SchemaGrant {

  def access
  def cubeGrant

  def xml = { builder ->
    builder.SchemaGrant('access': access) {
      cubeGrant?.xml(builder)
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    access = root.@access

    if (root.CubeGrant.size()) {
      cubeGrant = new CubeGrant();
      cubeGrant.load(XmlUtil.serialize(root.CubeGrant));
    }
  }
}
