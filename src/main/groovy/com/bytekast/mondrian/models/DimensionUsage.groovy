package com.bytekast.mondrian.models

class DimensionUsage {

  def name
  def source
  def foreignKey

  def xml = { builder ->
    builder.DimensionUsage('name': name, 'source': source, 'foreignKey': foreignKey)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
    source = root.@source
    foreignKey = root.@foreignKey
  }
}
