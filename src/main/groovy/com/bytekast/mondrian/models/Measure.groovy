package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

/**
 * @author Rowell Belen
 */
class Measure {

  def name
  def column
  def aggregator
  def formatString
  def calculatedMemberProperty

  def xml = { builder ->
    builder.Measure('name': name, 'column': column, 'aggregator': aggregator, 'formatString': formatString) {
      calculatedMemberProperty?.xml(builder)
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
    column = root.@column
    aggregator = root.@aggregator
    formatString = root.@formatString

    if (root.CalculatedMemberProperty.size()) {
      calculatedMemberProperty = new CalculatedMemberProperty();
      calculatedMemberProperty.load(XmlUtil.serialize(root.CalculatedMemberProperty));
    }
  }
}
