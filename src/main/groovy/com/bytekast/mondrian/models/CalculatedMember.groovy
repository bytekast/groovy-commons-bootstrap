package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

/**
 * @author Rowell Belen
 */
class CalculatedMember {

  def name
  def dimension
  def visible
  def formula
  def calculatedMemberProperties = []

  def xml = { builder ->
    builder.CalculatedMember('name': name, 'dimension': dimension, 'visible': visible) {
      formula?.xml(builder)

      calculatedMemberProperties.each {
        it.xml(builder)
      }
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
    dimension = root.@dimension
    visible = root.@visible

    if (root.Formula.size()) {
      formula = new Formula()
      formula.load(XmlUtil.serialize(root.Formula))
    }

    calculatedMemberProperties = []
    root.CalculatedMemberProperty.each {
      def obj = new CalculatedMemberProperty()
      obj.load(XmlUtil.serialize(it))
      calculatedMemberProperties << obj
    }
  }
}
