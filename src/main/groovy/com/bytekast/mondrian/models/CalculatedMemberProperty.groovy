package com.bytekast.mondrian.models

/**
 * @author Rowell Belen
 */
class CalculatedMemberProperty {

  def name
  def value

  def xml = { builder ->
    builder.CalculatedMemberProperty('name': name, 'value': value)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
    value = root.@value
  }
}
