package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

class Cube {

  String name
  def table
  def annotations
  def dimensionUsages = []
  def measures = []
  def calculatedMembers = []
  def namedSets = []

  def xml = { builder ->
    builder.Cube('name': name) {

      table?.xml(builder)

      annotations?.xml(builder)

      dimensionUsages.each {
        it.xml(builder)
      }

      measures.each {
        it.xml(builder)
      }

      calculatedMembers.each {
        it.xml(builder)
      }

      namedSets.each {
        it.xml(builder)
      }
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name

    if (root.Table.size()) {
      table = new Table()
      table.load(XmlUtil.serialize(root.Table))
    }

    if (root.Annotations.size()) {
      annotations = new Annotations()
      annotations.load(XmlUtil.serialize(root.Annotations))
    }

    dimensionUsages = []
    root.DimensionUsage.each {
      def obj = new DimensionUsage()
      obj.load(XmlUtil.serialize(it))
      dimensionUsages << obj
    }

    measures = []
    root.Measure.each {
      def obj = new Measure()
      obj.load(XmlUtil.serialize(it))
      measures << obj
    }

    calculatedMembers = []
    root.CalculatedMember.each {
      def obj = new CalculatedMember()
      obj.load(XmlUtil.serialize(it))
      calculatedMembers << obj
    }

    namedSets = []
    root.NamedSet.each {
      def obj = new NamedSet()
      obj.load(XmlUtil.serialize(it))
      namedSets << obj
    }
  }
}
