package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

/**
 * @author Rowell Belen
 */
class Role {

  def name
  def schemaGrant

  def xml = { builder ->
    builder.Role('name': name) {
      schemaGrant?.xml(builder)
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name

    if (root.SchemaGrant.size()) {
      schemaGrant = new SchemaGrant();
      schemaGrant.load(XmlUtil.serialize(root.SchemaGrant));
    }
  }
}
