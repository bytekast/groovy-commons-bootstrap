package com.bytekast.mondrian.models

import groovy.xml.XmlUtil

/**
 * @author Rowell Belen
 */
class VirtualCube {

  def name
  def defaultMeasure
  def virtualCubeDimensions = []
  def virtualCubeMeasures = []
  def calculatedMembers = []

  def xml = { builder ->
    builder.VirtualCube('name': name, 'defaultMeasure': defaultMeasure) {
      virtualCubeDimensions.each {
        it.xml(builder)
      }
      virtualCubeMeasures.each {
        it.xml(builder)
      }
      calculatedMembers.each {
        it.xml(builder)
      }
    }
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
    defaultMeasure = root.@defaultMeasure

    virtualCubeDimensions = []
    root.VirtualCubeDimension.each {
      def obj = new VirtualCubeDimension()
      obj.load(XmlUtil.serialize(it))
      virtualCubeDimensions << obj
    }

    virtualCubeMeasures = []
    root.VirtualCubeMeasure.each {
      def obj = new VirtualCubeMeasure()
      obj.load(XmlUtil.serialize(it))
      virtualCubeMeasures << obj
    }

    calculatedMembers = []
    root.CalculatedMember.each {
      def obj = new CalculatedMember()
      obj.load(XmlUtil.serialize(it))
      calculatedMembers << obj
    }
  }
}
