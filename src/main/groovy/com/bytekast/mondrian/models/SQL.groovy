package com.bytekast.mondrian.models

class SQL {

  def dialect
  def text

  def xml = { builder ->
    builder.SQL('dialect': dialect, text)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml)
    dialect = root.@dialect
    text = root.text()
  }
}
