package com.bytekast.mondrian.models

class AggForeignKey {

  def factColumn
  def aggColumn

  def xml = { builder ->
    builder.AggForeignKey('factColumn': factColumn, 'aggColumn': aggColumn)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    factColumn = root.@factColumn
    aggColumn = root.@aggColumn
  }
}
