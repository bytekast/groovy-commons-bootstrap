package com.bytekast.mondrian.models

class AggIgnoreColumn {

  def column

  def xml = { builder ->
    builder.AggIgnoreColumn('column': column)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    column = root.@column
  }
}
