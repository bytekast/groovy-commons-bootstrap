package com.bytekast.mondrian.models

class Annotation {

  def name
  def text

  def xml = { builder ->
    builder.Annotation('name': name, text)
  }

  def load = { xml ->
    def root = new XmlSlurper().parseText(xml);
    name = root.@name
    text = root.text()
  }
}
