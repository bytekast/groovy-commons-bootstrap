def books = '''
  <response version-api="2.0">
      <value>
          <books>
              <book available="20" id="1">
                  <title>Don Xijote</title>
                  <author id="1">Manuel De Cervantes</author>
              </book>
              <book available="14" id="2">
                  <title>Catcher in the Rye</title>
                 <author id="2">JD Salinger</author>
             </book>
             <book available="13" id="3">
                 <title>Alice in Wonderland</title>
                 <author id="3">Lewis Carroll</author>
             </book>
             <book available="5" id="4">
                 <title>Don Xijote</title>
                 <author id="4">Manuel De Cervantes</author>
             </book>
         </books>
     </value>
  </response>
'''

def response = new XmlSlurper().parseText(books)
def authorResult = response.value.books.book[0].author

assert authorResult.text() == 'Manuel De Cervantes'

def book = response.value.books.book[0] 
def bookAuthorId1 = book.@id 
def bookAuthorId2 = book['@id'] 

assert bookAuthorId1 == '1' 
assert bookAuthorId1.toInteger() == 1 
assert bookAuthorId1 == bookAuthorId2



response = new XmlSlurper().parseText(books)

def catcherInTheRye = response.value.books.'*'.find { node->
 /* node.@id == 2 could be expressed as node['@id'] == 2 */
    node.name() == 'book' && node.@id == '2'
}

assert catcherInTheRye.title.text() == 'Catcher in the Rye'


/////////////////////


response = new XmlSlurper().parseText(books)

def bookId = response.'**'.find { book1->
    book1.author.text() == 'Lewis Carroll'
}.@id

assert bookId == 3

////////////////////


response = new XmlSlurper().parseText(books)

def titles = response.'**'.findAll{ node-> node.name() == 'title' }*.text()

assert titles.size() == 4

////////////////////


titles = response.value.books.book.findAll{book1->
 /* You can use toInteger() over the GPathResult object */
    book1.@id.toInteger() > 2
}*.title

assert titles.size() == 2

