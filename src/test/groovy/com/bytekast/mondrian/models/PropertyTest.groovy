package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test


class PropertyTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    Property property = new Property();
    property.with {
      name = "Education"
      column = "education"
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    property.xml(builder);

    assert compareXml(writer.toString(), "<Property name=\"Education\" column=\"education\"/>")
  }

  @Test
  void testLoad() {
    def xml = "<Property name=\"Education\" column=\"education\"/>";
    Property property = new Property()
    property.load(xml)

    assert property.name == "Education"
    assert property.column == "education"
  }
}
