package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test


class AggFactCountTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def aggFactCount = new AggFactCount()
    aggFactCount.column = "FACT_COUNT"

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    aggFactCount.xml(builder);

    assert compareXml(writer.toString(), "<AggFactCount column=\"FACT_COUNT\"/>")
  }

  @Test
  void testLoad() {
    def xml = "<AggFactCount column=\"FACT_COUNT\"/>";
    def aggFactCount = new AggFactCount()
    aggFactCount.load(xml)

    assert aggFactCount.column == "FACT_COUNT"
  }
}
