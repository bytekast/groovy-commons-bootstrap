package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test


class AggExcludeTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    AggExclude aggExclude = new AggExclude()
    aggExclude.name = "agg_pc_10_sales_fact_1997"

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    aggExclude.xml(builder);

    assert compareXml(writer.toString(), "<AggExclude name=\"agg_pc_10_sales_fact_1997\"/>")
  }

  @Test
  void testLoad() {
    def xml = "<AggExclude name=\"agg_pc_10_sales_fact_1997\"/>";
    AggExclude aggExclude = new AggExclude()
    aggExclude.load(xml)

    assert aggExclude.name == "agg_pc_10_sales_fact_1997"
  }
}
