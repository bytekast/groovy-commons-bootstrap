package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class VirtualCubeMeasureTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    VirtualCubeMeasure virtualCubeMeasure = new VirtualCubeMeasure()
    virtualCubeMeasure.with {
      cubeName = "Warehouse"
      name = "[Measures].[Average Warehouse Sale]"
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    virtualCubeMeasure.xml(builder);

    assert compareXml(writer.toString(),
        "<VirtualCubeMeasure cubeName=\"Warehouse\" name=\"[Measures].[Average Warehouse Sale]\"/>")
  }

  @Test
  void testLoad() {

    def xml = "<VirtualCubeMeasure cubeName=\"Warehouse\" name=\"[Measures].[Average Warehouse Sale]\"/>"

    VirtualCubeMeasure virtualCubeMeasure = new VirtualCubeMeasure()
    virtualCubeMeasure.load(xml)

    assert virtualCubeMeasure.cubeName == "Warehouse"
    assert virtualCubeMeasure.name == "[Measures].[Average Warehouse Sale]"
  }
}
