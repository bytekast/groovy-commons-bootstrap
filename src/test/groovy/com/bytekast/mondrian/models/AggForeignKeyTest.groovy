package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test


class AggForeignKeyTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def aggForeignKey = new AggForeignKey()
    aggForeignKey.factColumn = "product_id"
    aggForeignKey.aggColumn = "PRODUCT_ID"

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    aggForeignKey.xml(builder);

    assert compareXml(writer.toString(), "<AggForeignKey factColumn=\"product_id\" aggColumn=\"PRODUCT_ID\" />")
  }

  @Test
  void testLoad() {
    def xml = "<AggForeignKey factColumn=\"product_id\" aggColumn=\"PRODUCT_ID\" />";
    def aggForeignKey = new AggForeignKey()
    aggForeignKey.load(xml)

    assert aggForeignKey.factColumn == "product_id"
    assert aggForeignKey.aggColumn == "PRODUCT_ID"
  }
}
