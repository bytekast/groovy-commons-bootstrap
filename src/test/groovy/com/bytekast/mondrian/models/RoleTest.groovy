package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

class RoleTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def cubeGrant = new CubeGrant();
    cubeGrant.with {
      access = 'none'
      cube = 'HR'
    }

    def schemaGrant = new SchemaGrant()
    schemaGrant.with {
      access = 'all'
      it.cubeGrant = cubeGrant
    }

    def role = new Role()
    role.with {
      name = 'No HR Cube'
      it.schemaGrant = schemaGrant
    }

    def writer = new StringWriter();
    def builder = new MarkupBuilder(writer);
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    role.xml(builder);

    assert compareXml(writer.toString(), """
      <Role name='No HR Cube'>
        <SchemaGrant access='all'>
          <CubeGrant access='none' cube='HR' />
        </SchemaGrant>
      </Role>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <Role name='No HR Cube'>
        <SchemaGrant access='all'>
          <CubeGrant access='none' cube='HR' />
        </SchemaGrant>
      </Role>
    """
    def role = new Role()
    role.load(xml)

    assert role.name == "No HR Cube";
    assert role.schemaGrant.access == "all"
    assert role.schemaGrant.cubeGrant.access == "none"
    assert role.schemaGrant.cubeGrant.cube == "HR"
  }
}
