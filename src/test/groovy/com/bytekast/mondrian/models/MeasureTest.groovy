package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class MeasureTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def calculatedMemberProperty = new CalculatedMemberProperty()
    calculatedMemberProperty.with {
      name = "MEMBER_ORDINAL"
      value = 7
    }

    def measure = new Measure()
    measure.with {
      name = "Customer Count"
      column = "customer_id"
      aggregator = "distinct-count"
      formatString = "#,###"
      it.calculatedMemberProperty = calculatedMemberProperty
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    measure.xml(builder);

    assert compareXml(writer.toString(), """
      <Measure name="Customer Count" column="customer_id" aggregator="distinct-count" formatString="#,###">
        <CalculatedMemberProperty name="MEMBER_ORDINAL" value="7"/>
      </Measure>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <Measure name="Customer Count" column="customer_id" aggregator="distinct-count" formatString="#,###">
        <CalculatedMemberProperty name="MEMBER_ORDINAL" value="7"/>
      </Measure>
    """

    def measure = new Measure()
    measure.load(xml)

    assert measure.name == "Customer Count"
    assert measure.column == "customer_id"
    assert measure.aggregator == "distinct-count"
    assert measure.formatString == "#,###"
    assert measure.calculatedMemberProperty.name == "MEMBER_ORDINAL"
    assert measure.calculatedMemberProperty.value == 7
  }
}
