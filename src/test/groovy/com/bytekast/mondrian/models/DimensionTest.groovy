package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class DimensionTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def table = new Table()
    table.name = "promotion"

    def level = new Level()
    level.with {
      name = "Media Type"
      column = "media_type"
      uniqueMembers = true
    }

    def hierarchy = new Hierarchy()
    hierarchy.with {
      hasAll = true
      allMemberName = "All Media"
      primaryKey = "promotion_id"
      defaultMember = "All Media"
      it.table = table
      levels = [level]
    }

    def dimension = new Dimension()
    dimension.with {
      name = "Promotion"
      type = ""
      foreignKey = "promotion_id"
      hierarchies = [hierarchy]
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitNullAttributes(true)
    builder.setExpandEmptyElements(false)
    dimension.xml(builder);

    assert compareXml(writer.toString(), """
      <Dimension name="Promotion" type="" foreignKey="promotion_id">
        <Hierarchy hasAll="true" allMemberName="All Media" primaryKey="promotion_id" defaultMember="All Media">
          <Table name="promotion"/>
          <Level name="Media Type" column="media_type" uniqueMembers="true"/>
        </Hierarchy>
      </Dimension>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <Dimension name="Promotion" type="" foreignKey="promotion_id">
        <Hierarchy hasAll="true" allMemberName="All Media" primaryKey="promotion_id" defaultMember="All Media">
          <Table name="promotion"/>
          <Level name="Media Type" column="media_type" uniqueMembers="true"/>
        </Hierarchy>
      </Dimension>
    """

    Dimension dimension = new Dimension()
    dimension.load(xml)

    assert dimension.name == "Promotion"
    assert dimension.type == ""
    assert dimension.foreignKey == "promotion_id"
    assert dimension.hierarchies[0].hasAll == true
    assert dimension.hierarchies[0].allMemberName == "All Media"
    assert dimension.hierarchies[0].primaryKey == "promotion_id"
    assert dimension.hierarchies[0].defaultMember == "All Media"
    assert dimension.hierarchies[0].table.name == "promotion"
    assert dimension.hierarchies[0].levels[0].name == "Media Type"
    assert dimension.hierarchies[0].levels[0].column == "media_type"
    assert dimension.hierarchies[0].levels[0].uniqueMembers == true
  }
}
