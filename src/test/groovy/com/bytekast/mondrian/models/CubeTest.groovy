package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test


class CubeTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    Measure measure1 = new Measure()
    measure1.with {
      name = "Store Invoice"
      column = "store_invoice"
      aggregator = "sum"
    }

    Measure measure2 = new Measure()
    measure2.with {
      name = "Supply Time"
      column = "supply_time"
      aggregator = "sum"
    }

    DimensionUsage dimensionUsage1 = new DimensionUsage();
    dimensionUsage1.with {
      name = "Product"
      source = "Product"
      foreignKey = "product_id"
    }

    DimensionUsage dimensionUsage2 = new DimensionUsage();
    dimensionUsage2.with {
      name = "Warehouse"
      source = "Warehouse"
      foreignKey = "warehouse_id"
    }

    Table table = new Table()
    table.name = "inventory_fact_1997"

    CalculatedMember calculatedMember = new CalculatedMember()
    calculatedMember.name = "Average Warehouse Sale"
    calculatedMember.dimension = "Measures"

    NamedSet namedSet = new NamedSet()
    namedSet.name = "Top Sellers"

    def annotation1 = new Annotation()
    annotation1.with {
      name = "caption.de_DE"
      text = "Verkaufen"
    }

    def annotation2 = new Annotation()
    annotation2.with {
      name = "caption.fr_FR"
      text = "Ventes"
    }

    def annotations = new Annotations()
    annotations.annotations = [annotation1, annotation2]

    Cube cube = new Cube();
    cube.with {
      name = "Warehouse"
      it.table = table
      dimensionUsages = [dimensionUsage1, dimensionUsage2]
      measures = [measure1, measure2]
      namedSets = [namedSet]
      calculatedMembers = [calculatedMember]
      it.annotations = annotations
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitNullAttributes(true)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    cube.xml(builder);

    assert compareXml(writer.toString(), """
    <Cube name="Warehouse">
      <Table name="inventory_fact_1997"/>
      <Annotations>
        <Annotation name="caption.de_DE">Verkaufen</Annotation>
        <Annotation name="caption.fr_FR">Ventes</Annotation>
      </Annotations>
      <DimensionUsage name="Product" source="Product" foreignKey="product_id"/>
      <DimensionUsage name="Warehouse" source="Warehouse" foreignKey="warehouse_id"/>
      <Measure name="Store Invoice" column="store_invoice" aggregator="sum"/>
      <Measure name="Supply Time" column="supply_time" aggregator="sum"/>
      <CalculatedMember name="Average Warehouse Sale" dimension="Measures"/>
      <NamedSet name="Top Sellers" />
    </Cube>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
    <Cube name="Warehouse">
      <Table name="inventory_fact_1997"/>
      <Annotations>
        <Annotation name="caption.de_DE">Verkaufen</Annotation>
        <Annotation name="caption.fr_FR">Ventes</Annotation>
      </Annotations>
      <DimensionUsage name="Product" source="Product" foreignKey="product_id"/>
      <DimensionUsage name="Warehouse" source="Warehouse" foreignKey="warehouse_id"/>
      <Measure name="Store Invoice" column="store_invoice" aggregator="sum"/>
      <Measure name="Supply Time" column="supply_time" aggregator="sum"/>
      <CalculatedMember name="Average Warehouse Sale" dimension="Measures"/>
      <NamedSet name="Top Sellers" />
    </Cube>
    """

    Cube cube = new Cube()
    cube.load(xml)

    assert cube.name == "Warehouse"
    assert cube.table.name == "inventory_fact_1997"
    assert cube.dimensionUsages[0].name == "Product"
    assert cube.dimensionUsages[0].source == "Product"
    assert cube.dimensionUsages[0].foreignKey == "product_id"
    assert cube.dimensionUsages[1].name == "Warehouse"
    assert cube.dimensionUsages[1].source == "Warehouse"
    assert cube.dimensionUsages[1].foreignKey == "warehouse_id"
    assert cube.measures[0].name == "Store Invoice"
    assert cube.measures[0].column == "store_invoice"
    assert cube.measures[0].aggregator == "sum"
    assert cube.measures[1].name == "Supply Time"
    assert cube.measures[1].column == "supply_time"
    assert cube.measures[1].aggregator == "sum"
    assert cube.calculatedMembers[0].name == "Average Warehouse Sale"
    assert cube.calculatedMembers[0].dimension == "Measures"
    assert cube.namedSets[0].name == "Top Sellers"
    assert cube.annotations.annotations[0].name == "caption.de_DE"
    assert cube.annotations.annotations[0].text == "Verkaufen"
    assert cube.annotations.annotations[1].name == "caption.fr_FR"
    assert cube.annotations.annotations[1].text == "Ventes"
  }
}
