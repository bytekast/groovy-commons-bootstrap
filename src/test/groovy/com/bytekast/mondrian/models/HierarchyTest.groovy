package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class HierarchyTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def table = new Table()
    table.name = "promotion"

    def level = new Level()
    level.with {
      name = "Media Type"
      column = "media_type"
      uniqueMembers = true
    }

    def hierarchy = new Hierarchy()
    hierarchy.with {
      hasAll = true
      allMemberName = "All Media"
      primaryKey = "promotion_id"
      defaultMember = "All Media"
      it.table = table
      levels = [level]
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setOmitNullAttributes(true)
    builder.setExpandEmptyElements(false)
    hierarchy.xml(builder);

    assert compareXml(writer.toString(), """
      <Hierarchy hasAll="true" allMemberName="All Media" primaryKey="promotion_id" defaultMember="All Media">
        <Table name="promotion"/>
        <Level name="Media Type" column="media_type" uniqueMembers="true"/>
      </Hierarchy>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <Hierarchy hasAll="true" allMemberName="All Media" primaryKey="promotion_id" defaultMember="All Media">
        <Table name="promotion"/>
        <Level name="Media Type" column="media_type" uniqueMembers="true"/>
      </Hierarchy>
    """

    Hierarchy hierarchy = new Hierarchy()
    hierarchy.load(xml)

    assert hierarchy.hasAll == true
    assert hierarchy.allMemberName == "All Media"
    assert hierarchy.primaryKey == "promotion_id"
    assert hierarchy.defaultMember == "All Media"
    assert hierarchy.table.name == "promotion"
    assert hierarchy.levels[0].name == "Media Type"
    assert hierarchy.levels[0].column == "media_type"
    assert hierarchy.levels[0].uniqueMembers == true
  }
}
