package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class CalculatedMemberPropertyTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def formula = new Formula()
    formula.text = "[Measures].[Profit] / [Measures].[Units Shipped]"

    def calculatedMemberProperty = new CalculatedMemberProperty()
    calculatedMemberProperty.name = "MEMBER_ORDINAL"
    calculatedMemberProperty.value = 5

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    calculatedMemberProperty.xml(builder);

    assert compareXml(writer.toString(), "<CalculatedMemberProperty name=\"MEMBER_ORDINAL\" value=\"5\"/>")
  }

  @Test
  void testLoad() {
    def xml = "<CalculatedMemberProperty name=\"MEMBER_ORDINAL\" value=\"5\"/>"

    def calculatedMemberProperty = new CalculatedMemberProperty()
    calculatedMemberProperty.load(xml)

    assert calculatedMemberProperty.name == "MEMBER_ORDINAL"
    assert calculatedMemberProperty.value == 5
  }
}
