package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class AnnotationTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def annotation = new Annotation()
    annotation.name = "description.fr_FR"
    annotation.text = "Cube des ventes"

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    annotation.xml(builder);

    assert compareXml(writer.toString(),
        "<Annotation name=\"description.fr_FR\">Cube des ventes</Annotation>")
  }

  @Test
  void testLoad() {
    def xml = "<Annotation name=\"description.fr_FR\">Cube des ventes</Annotation>";
    def annotation = new Annotation()
    annotation.load(xml)

    assert annotation.name == "description.fr_FR"
    assert annotation.text == "Cube des ventes"
  }
}
