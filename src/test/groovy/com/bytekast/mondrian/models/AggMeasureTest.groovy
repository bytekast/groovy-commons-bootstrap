package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test


class AggMeasureTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def model = new AggMeasure()
    model.name = "[Measures].[Store Sales]"
    model.column = "STORE_SALES_SUM"

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    model.xml(builder);

    assert compareXml(writer.toString(), "<AggMeasure name=\"[Measures].[Store Sales]\" column=\"STORE_SALES_SUM\" />")
  }

  @Test
  void testLoad() {
    def xml = "<AggMeasure name=\"[Measures].[Store Sales]\" column=\"STORE_SALES_SUM\" />";
    def model = new AggMeasure()
    model.load(xml)

    assert model.name == "[Measures].[Store Sales]"
    assert model.column == "STORE_SALES_SUM"
  }
}
