package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test


class AggIgnoreColumnTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def aggIgnoreColumn = new AggIgnoreColumn()
    aggIgnoreColumn.column = "foo"

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    aggIgnoreColumn.xml(builder);

    assert compareXml(writer.toString(), "<AggIgnoreColumn column=\"foo\"/>")
  }

  @Test
  void testLoad() {
    def xml = "<AggIgnoreColumn column=\"foo\"/>";
    def aggIgnoreColumn = new AggIgnoreColumn()
    aggIgnoreColumn.load(xml)

    assert aggIgnoreColumn.column == "foo"
  }
}
