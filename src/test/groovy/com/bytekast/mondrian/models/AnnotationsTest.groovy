package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class AnnotationsTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def annotation1 = new Annotation()
    annotation1.with {
      name = "caption.de_DE"
      text = "Verkaufen"
    }

    def annotation2 = new Annotation()
    annotation2.with {
      name = "caption.fr_FR"
      text = "Ventes"
    }

    def annotations = new Annotations()
    annotations.annotations = [annotation1, annotation2]

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    annotations.xml(builder)

    assert compareXml(writer.toString(), """
      <Annotations>
        <Annotation name="caption.de_DE">Verkaufen</Annotation>
        <Annotation name="caption.fr_FR">Ventes</Annotation>
      </Annotations>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <Annotations>
        <Annotation name="caption.de_DE">Verkaufen</Annotation>
        <Annotation name="caption.fr_FR">Ventes</Annotation>
      </Annotations>
    """

    def annotations = new Annotations()
    annotations.load(xml)

    assert annotations.annotations[0].name == "caption.de_DE"
    assert annotations.annotations[0].text == "Verkaufen"
    assert annotations.annotations[1].name == "caption.fr_FR"
    assert annotations.annotations[1].text == "Ventes"
  }
}
