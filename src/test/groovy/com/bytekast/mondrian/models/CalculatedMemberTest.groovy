package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class CalculatedMemberTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def calculatedMemberProperty = new CalculatedMemberProperty()
    calculatedMemberProperty.name = "MEMBER_ORDINAL"
    calculatedMemberProperty.value = 5

    def formula = new Formula()
    formula.text = "[Measures].[Profit] / [Measures].[Units Shipped]"

    def calculatedMember = new CalculatedMember()
    calculatedMember.with {
      name = "Profit Per Unit Shipped"
      dimension = "Measures"
      it.formula = formula
      visible = false
      calculatedMemberProperties = [calculatedMemberProperty]
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    calculatedMember.xml(builder);

    assert compareXml(writer.toString(), """
      <CalculatedMember name="Profit Per Unit Shipped" dimension="Measures" visible="false">
        <Formula>[Measures].[Profit] / [Measures].[Units Shipped]</Formula>
        <CalculatedMemberProperty name="MEMBER_ORDINAL" value="5"/>
      </CalculatedMember>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <CalculatedMember name="Profit Per Unit Shipped" dimension="Measures" visible="true">
        <Formula>[Measures].[Profit] / [Measures].[Units Shipped]</Formula>
        <CalculatedMemberProperty name="MEMBER_ORDINAL" value="5"/>
      </CalculatedMember>
    """

    CalculatedMember calculatedMember = new CalculatedMember()
    calculatedMember.load(xml)

    assert calculatedMember.name == "Profit Per Unit Shipped"
    assert calculatedMember.dimension == "Measures"
    assert calculatedMember.visible == true
    assert calculatedMember.formula.text == "[Measures].[Profit] / [Measures].[Units Shipped]"
    assert calculatedMember.calculatedMemberProperties[0].name == "MEMBER_ORDINAL"
    assert calculatedMember.calculatedMemberProperties[0].value == 5
  }
}
