package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class KeyExpressionTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def neoview = new SQL()
    neoview.dialect = "neoview"
    neoview.text = """
      "customer"."fullname"
    """

    def generic = new SQL()
    generic.dialect = "generic"
    generic.text = "fullname"

    KeyExpression keyExpression = new KeyExpression()
    keyExpression.sqlList = [neoview, generic]

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setOmitNullAttributes(true)
    builder.setExpandEmptyElements(false)
    keyExpression.xml(builder);

    assert compareXml(writer.toString(), """
      <KeyExpression>
        <SQL dialect="neoview">
          "customer"."fullname"
        </SQL>
        <SQL dialect="generic">
          fullname
        </SQL>
      </KeyExpression>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <KeyExpression>
        <SQL dialect="neoview">
          "customer"."fullname"
        </SQL>
        <SQL dialect="generic">
          fullname
        </SQL>
      </KeyExpression>
    """

    KeyExpression keyExpression = new KeyExpression()
    keyExpression.load(xml)

    assert keyExpression.sqlList[0].dialect == "neoview"
    assert keyExpression.sqlList[0].text.trim() == "\"customer\".\"fullname\""

    assert keyExpression.sqlList[1].dialect == "generic"
    assert keyExpression.sqlList[1].text.trim() == "fullname"
  }
}
