package com.bytekast.mondrian.models

import org.junit.Test

class SchemaGrantTest extends XmlGroovyTestCase {

  @Test
  void testLoad() {
    def xml = """
    <SchemaGrant access='all'>
      <CubeGrant access='none' cube='HR' />
    </SchemaGrant>
    """
    def schemaGrant = new SchemaGrant()
    schemaGrant.load(xml)

    assertEquals("all", schemaGrant.access.toString())
  }
}
