package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test


class DimensionUsageTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    DimensionUsage dimensionUsage = new DimensionUsage();
    dimensionUsage.with{
      name = "Time"
      source = "Time"
      foreignKey = "time_id"
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    dimensionUsage.xml(builder);

    assert compareXml(writer.toString(), "<DimensionUsage name=\"Time\" source=\"Time\" foreignKey=\"time_id\"/>")
  }

  @Test
  void testLoad() {
    def xml = "<DimensionUsage name=\"Time\" source=\"Time\" foreignKey=\"time_id\"/>";
    DimensionUsage dimensionUsage = new DimensionUsage()
    dimensionUsage.load(xml)

    assert dimensionUsage.name == "Time"
    assert dimensionUsage.source == "Time"
    assert dimensionUsage.foreignKey == "time_id"
  }
}
