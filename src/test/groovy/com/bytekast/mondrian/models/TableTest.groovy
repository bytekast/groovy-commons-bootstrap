package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test


class TableTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    Table table = new Table()
    table.name = "sales_fact_1997"

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    table.xml(builder);

    assert compareXml(writer.toString(), "<Table name=\"sales_fact_1997\"/>")
  }

  @Test
  void testLoad() {
    def xml = "<Table name=\"sales_fact_1997\"/>";
    Table table = new Table()
    table.load(xml)

    assert table.name == "sales_fact_1997"
  }
}
