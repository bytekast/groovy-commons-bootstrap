package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test


class LevelTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def neoview = new SQL()
    neoview.dialect = "neoview"
    neoview.text = """
      "customer"."fullname"
    """

    def table = new Table()
    table.name = "employee_closure"

    def closure = new Closure()
    closure.with {
      parentColumn = "supervisor_id"
      childColumn = "employee_id"
      it.table = table
    }

    def generic = new SQL()
    generic.dialect = "generic"
    generic.text = "fullname"

    KeyExpression keyExpression = new KeyExpression()
    keyExpression.sqlList = [neoview, generic]

    Property property = new Property();
    property.name = "Education"
    property.column = "education"

    Level level = new Level();
    level.with {
      name = "Gender"
      column = "gender"
      uniqueMembers = true
      it.closure = closure
      keyExpressions = [keyExpression]
      properties = [property]
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitNullAttributes(true)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    level.xml(builder);

    assert compareXml(writer.toString(), """
      <Level name="Gender" column="gender" uniqueMembers="true">
        <Closure parentColumn="supervisor_id" childColumn="employee_id">
          <Table name="employee_closure"/>
        </Closure>
        <Property name="Education" column="education" />
        <KeyExpression>
          <SQL dialect="neoview">"customer"."fullname"</SQL>
          <SQL dialect="generic">fullname</SQL>
        </KeyExpression>
      </Level>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <Level name="Gender" column="gender" uniqueMembers="true">
        <Closure />
        <Property name="Education" column="education" />
        <KeyExpression>
          <SQL dialect="neoview">"customer"."fullname"</SQL>
          <SQL dialect="generic">fullname</SQL>
        </KeyExpression>
      </Level>
    """

    Level level = new Level()
    level.load(xml)

    assert level.name == "Gender"
    assert level.column == "gender"
    assert level.uniqueMembers == true

    assert level.properties[0].name == "Education"
    assert level.properties[0].column == "education"

    assert level.keyExpressions[0].sqlList[0].dialect == "neoview"
    assert level.keyExpressions[0].sqlList[0].text.trim() == "\"customer\".\"fullname\""
    assert level.keyExpressions[0].sqlList[1].dialect == "generic"
    assert level.keyExpressions[0].sqlList[1].text.trim() == "fullname"

    assert level.closure != null
  }
}
