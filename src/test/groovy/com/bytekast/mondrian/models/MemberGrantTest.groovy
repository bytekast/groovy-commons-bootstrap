package com.bytekast.mondrian.models

import org.junit.Test

class MemberGrantTest extends XmlGroovyTestCase {

  @Test
  void testLoad() {

    def xml = "<MemberGrant access='all' member='[Customers].[USA].[CA].[Los Angeles]' />"
    def memberGrant = new MemberGrant()
    memberGrant.load(xml)

    assert "all" == memberGrant.access.toString()
    assert "[Customers].[USA].[CA].[Los Angeles]" == memberGrant.member.toString()
  }
}
