package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class FormulaTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    Formula formula = new Formula()
    formula.text = "[Measures].[Profit] / [Measures].[Units Shipped]"

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    formula.xml(builder);

    assert compareXml(writer.toString(),
        "<Formula>[Measures].[Profit] / [Measures].[Units Shipped]</Formula>")
  }

  @Test
  void testLoad() {
    def xml = "<Formula>[Measures].[Profit] / [Measures].[Units Shipped]</Formula>";
    Formula formula = new Formula()
    formula.load(xml)

    assert formula.text == "[Measures].[Profit] / [Measures].[Units Shipped]"
  }
}
