package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class ClosureTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def table = new Table()
    table.name = "employee_closure"

    def closure = new Closure()
    closure.with {
      parentColumn = "supervisor_id"
      childColumn = "employee_id"
      it.table = table
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setOmitNullAttributes(true)
    builder.setExpandEmptyElements(false)
    closure.xml(builder);

    assert compareXml(writer.toString(), """
      <Closure parentColumn="supervisor_id" childColumn="employee_id">
        <Table name="employee_closure"/>
      </Closure>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <Closure parentColumn="supervisor_id" childColumn="employee_id">
        <Table name="employee_closure"/>
      </Closure>
    """

    def closure = new Closure()
    closure.load(xml)

    assert closure.parentColumn == "supervisor_id"
    assert closure.childColumn == "employee_id"
    assert closure.table.name == "employee_closure"
  }

  @Test
  void testLoadMissingTable() {
    def xml = """
      <Closure parentColumn="supervisor_id" childColumn="employee_id" />
    """

    def closure = new Closure()
    closure.load(xml)

    assert closure.parentColumn == "supervisor_id"
    assert closure.childColumn == "employee_id"
    assert closure.table == null
  }
}
