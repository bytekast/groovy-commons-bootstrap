package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test


class AggLevelTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def model = new AggLevel()
    model.name = "[Time].[Year]"
    model.column = "TIME_YEAR"

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    model.xml(builder);

    assert compareXml(writer.toString(), "<AggLevel name=\"[Time].[Year]\" column=\"TIME_YEAR\" />")
  }

  @Test
  void testLoad() {
    def xml = "<AggLevel name=\"[Time].[Year]\" column=\"TIME_YEAR\" />";
    def model = new AggLevel()
    model.load(xml)

    assert model.name == "[Time].[Year]"
    assert model.column == "TIME_YEAR"
  }
}
