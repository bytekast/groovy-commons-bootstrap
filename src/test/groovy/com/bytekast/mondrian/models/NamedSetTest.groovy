package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class NamedSetTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    Formula formula = new Formula()
    formula.text = "TopCount([Warehouse].[Warehouse Name].MEMBERS, 5, [Measures].[Warehouse Sales])"

    NamedSet namedSet = new NamedSet();
    namedSet.with {
      name = "Top Sellers"
      it.formula = formula
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    namedSet.xml(builder);

    assert compareXml(writer.toString(), """
      <NamedSet name="Top Sellers">
        <Formula>TopCount([Warehouse].[Warehouse Name].MEMBERS, 5, [Measures].[Warehouse Sales])</Formula>
      </NamedSet>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <NamedSet name="Top Sellers">
        <Formula>TopCount([Warehouse].[Warehouse Name].MEMBERS, 5, [Measures].[Warehouse Sales])</Formula>
      </NamedSet>
    """

    def namedSet = new NamedSet()
    namedSet.load(xml)

    assert namedSet.name == "Top Sellers"
    assert namedSet.formula.text == "TopCount([Warehouse].[Warehouse Name].MEMBERS, 5, [Measures].[Warehouse Sales])"
  }
}
