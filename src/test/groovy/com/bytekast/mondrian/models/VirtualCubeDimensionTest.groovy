package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class VirtualCubeDimensionTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    VirtualCubeDimension virtualCubeDimension = new VirtualCubeDimension()
    virtualCubeDimension.with{
      cubeName = "Sales"
      name = "Promotion Media"
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    virtualCubeDimension.xml(builder);

    assert compareXml(writer.toString(),
        "<VirtualCubeDimension cubeName=\"Sales\" name=\"Promotion Media\"/>")
  }

  @Test
  void testLoad() {

    def xml = "<VirtualCubeDimension cubeName=\"Sales\" name=\"Promotion Media\"/>"

    VirtualCubeDimension virtualCubeDimension = new VirtualCubeDimension()
    virtualCubeDimension.load(xml)

    assert virtualCubeDimension.cubeName == "Sales"
    assert virtualCubeDimension.name == "Promotion Media"
  }
}
