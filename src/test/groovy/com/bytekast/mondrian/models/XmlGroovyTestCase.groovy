package com.bytekast.mondrian.models

import org.custommonkey.xmlunit.XMLUnit
import org.junit.Ignore

/**
 * @author Rowell Belen
 */
@Ignore
class XmlGroovyTestCase extends GroovyTestCase {

  def compareXml = { expectedXml, actualXml ->
    XMLUnit.setIgnoreWhitespace(true)
    XMLUnit.setIgnoreComments(true)
    XMLUnit.setIgnoreDiffBetweenTextAndCDATA(true)
    XMLUnit.setIgnoreAttributeOrder(true)
    XMLUnit.setNormalizeWhitespace(true)

    return XMLUnit.compareXML(expectedXml, actualXml).identical();
  }
}
