package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class JoinTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def t1 = new Table()
    t1.name = "employee"

    def t2 = new Table()
    t2.name = "store"

    def join = new Join()
    join.with {
      leftKey = "store_id"
      rightKey = "store_id"
      tables = [t1, t2]
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    join.xml(builder);

    assert compareXml(writer.toString(), """
      <Join leftKey="store_id" rightKey="store_id">
        <Table name="employee"/>
        <Table name="store"/>
      </Join>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <Join leftKey="store_id" rightKey="store_id">
        <Table name="employee"/>
        <Table name="store"/>
      </Join>
    """

    Join join = new Join()
    join.load(xml)

    assert join.leftKey == "store_id"
    assert join.rightKey == "store_id"
    assert join.tables[0].name == "employee"
    assert join.tables[1].name == "store"
  }
}
