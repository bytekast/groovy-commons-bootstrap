package com.bytekast.mondrian.models

import org.junit.Test

class CubeGrantTest extends XmlGroovyTestCase {

  @Test
  void testLoad() {

    def xml = "<CubeGrant access='none' cube='HR' />"
    def cubeGrant = new CubeGrant();
    cubeGrant.load(xml);

    assert "none" == cubeGrant.access.toString()
    assert "HR" == cubeGrant.cube.toString()
  }
}
