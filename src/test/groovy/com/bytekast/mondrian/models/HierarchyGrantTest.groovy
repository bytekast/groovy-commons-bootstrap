package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class HierarchyGrantTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    MemberGrant memberGrant = new MemberGrant();
    memberGrant.access = 'all'
    memberGrant.member = '[Customers].[USA].[CA].[Los Angeles]'

    HierarchyGrant hierarchyGrant = new HierarchyGrant();
    hierarchyGrant.with{
      access = "custom"
      bottomLevel = "[Customers].[City]"
      hierarchy = '[Customers]'
      topLevel = '[Customers].[State Province]'
      memberGrants = [memberGrant]
    }

    def writer = new StringWriter();
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    hierarchyGrant.xml(builder);

    assert compareXml(writer.toString(), """
    <HierarchyGrant access='custom' hierarchy='[Customers]' bottomLevel='[Customers].[City]'
      topLevel='[Customers].[State Province]'>
      <MemberGrant access='all' member='[Customers].[USA].[CA].[Los Angeles]' />
    </HierarchyGrant>
    """)
  }

  @Test
  void testLoad() {

    def xml = """
      <HierarchyGrant access='custom' hierarchy='[Customers]' bottomLevel='[Customers].[City]'
      topLevel='[Customers].[State Province]'>
      <MemberGrant access='all' member='[Customers].[USA].[CA].[Los Angeles]' />
    </HierarchyGrant>
    """

    HierarchyGrant hierarchyGrant = new HierarchyGrant()
    hierarchyGrant.load(xml)

    assert hierarchyGrant.access == "custom"
    assert hierarchyGrant.hierarchy == "[Customers]"
    assert hierarchyGrant.bottomLevel == "[Customers].[City]"
    assert hierarchyGrant.topLevel == "[Customers].[State Province]"
    assert hierarchyGrant.memberGrants[0].access == "all"
    assert hierarchyGrant.memberGrants[0].member == "[Customers].[USA].[CA].[Los Angeles]"
  }
}
