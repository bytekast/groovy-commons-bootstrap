package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class VirtualCubeTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def formula = new Formula()
    formula.text = "[Measures].[Profit] / [Measures].[Units Shipped]"

    def calculatedMember = new CalculatedMember()
    calculatedMember.with {
      name = "Profit Per Unit Shipped"
      dimension = "Measures"
      it.formula = formula
    }

    VirtualCubeDimension virtualCubeDimension = new VirtualCubeDimension()
    virtualCubeDimension.with {
      cubeName = "Sales"
      name = "Customers"
    }


    VirtualCubeMeasure virtualCubeMeasure = new VirtualCubeMeasure()
    virtualCubeMeasure.with {
      cubeName = "Warehouse"
      name = "[Measures].[Average Warehouse Sale]"
    }

    VirtualCube virtualCube = new VirtualCube()
    virtualCube.with {
      name = "Warehouse and Sales"
      defaultMeasure = "Store Sales"
      virtualCubeDimensions = [virtualCubeDimension]
      virtualCubeMeasures = [virtualCubeMeasure]
      calculatedMembers = [calculatedMember]
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setOmitNullAttributes(true)
    builder.setExpandEmptyElements(false)
    virtualCube.xml(builder);

    assert compareXml(writer.toString(), """
      <VirtualCube name="Warehouse and Sales" defaultMeasure="Store Sales">
        <VirtualCubeDimension cubeName="Sales" name="Customers"/>
        <VirtualCubeMeasure cubeName="Warehouse" name="[Measures].[Average Warehouse Sale]"/>
        <CalculatedMember name="Profit Per Unit Shipped" dimension="Measures">
          <Formula>[Measures].[Profit] / [Measures].[Units Shipped]</Formula>
        </CalculatedMember>
      </VirtualCube>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <VirtualCube name="Warehouse and Sales" defaultMeasure="Store Sales">
        <VirtualCubeDimension cubeName="Sales" name="Customers"/>
        <VirtualCubeMeasure cubeName="Warehouse" name="[Measures].[Average Warehouse Sale]"/>
        <CalculatedMember name="Profit Per Unit Shipped" dimension="Measures">
          <Formula>[Measures].[Profit] / [Measures].[Units Shipped]</Formula>
        </CalculatedMember>
      </VirtualCube>
    """

    VirtualCube virtualCube = new VirtualCube()
    virtualCube.load(xml)

    assert virtualCube.name == "Warehouse and Sales"
    assert virtualCube.defaultMeasure == "Store Sales"
    assert virtualCube.virtualCubeDimensions[0].cubeName == "Sales"
    assert virtualCube.virtualCubeDimensions[0].name == "Customers"

    assert virtualCube.virtualCubeMeasures[0].cubeName == "Warehouse"
    assert virtualCube.virtualCubeMeasures[0].name == "[Measures].[Average Warehouse Sale]"

    assert virtualCube.calculatedMembers[0].name == "Profit Per Unit Shipped"
    assert virtualCube.calculatedMembers[0].dimension == "Measures"
  }
}
