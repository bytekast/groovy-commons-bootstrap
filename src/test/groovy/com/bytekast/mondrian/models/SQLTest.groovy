package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test


class SQLTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    SQL sql = new SQL()
    sql.with {
      dialect = "postgres"
      sql.text = """
      "fname" || ' ' || "lname"
      """
    }

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setExpandEmptyElements(false)
    sql.xml(builder);

    assert compareXml(writer.toString(), """
    <SQL dialect="postgres">
      "fname" || ' ' || "lname"
    </SQL>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
    <SQL dialect="postgres">
      "fname" || ' ' || "lname"
    </SQL>
    """
    SQL sql = new SQL()
    sql.load(xml)

    assert sql.dialect == "postgres"
    assert sql.text == """
      "fname" || ' ' || "lname"
    """
  }
}
