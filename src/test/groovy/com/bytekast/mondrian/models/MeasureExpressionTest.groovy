package com.bytekast.mondrian.models

import groovy.xml.MarkupBuilder
import org.junit.Test

/**
 * @author Rowell Belen
 */
class MeasureExpressionTest extends XmlGroovyTestCase {

  @Test
  void testXml() {

    def oracle = new SQL()
    oracle.dialect = "oracle"
    oracle.text = """
      (case when "sales_fact_1997"."promotion_id" = 0 then 0 else "sales_fact_1997"."store_sales" end)
    """

    def hsqldb = new SQL()
    hsqldb.dialect = "hsqldb"
    hsqldb.text = """
      (case when "sales_fact_1997"."promotion_id" = 0 then 0 else "sales_fact_1997"."store_sales" end)
    """

    MeasureExpression expression = new MeasureExpression()
    expression.sqlList = [oracle, hsqldb]

    def writer = new StringWriter()
    def builder = new MarkupBuilder(writer)
    builder.setOmitEmptyAttributes(true)
    builder.setOmitNullAttributes(true)
    builder.setExpandEmptyElements(false)
    expression.xml(builder);

    assert compareXml(writer.toString(), """
      <MeasureExpression>
        <SQL dialect="oracle">
          (case when "sales_fact_1997"."promotion_id" = 0 then 0 else "sales_fact_1997"."store_sales" end)
        </SQL>
        <SQL dialect="hsqldb">
          (case when "sales_fact_1997"."promotion_id" = 0 then 0 else "sales_fact_1997"."store_sales" end)
        </SQL>
      </MeasureExpression>
    """)
  }

  @Test
  void testLoad() {
    def xml = """
      <MeasureExpression>
        <SQL dialect="oracle">
          (case when "sales_fact_1997"."promotion_id" = 0 then 0 else "sales_fact_1997"."store_sales" end)
        </SQL>
        <SQL dialect="hsqldb">
          (case when "sales_fact_1997"."promotion_id" = 0 then 0 else "sales_fact_1997"."store_sales" end)
        </SQL>
      </MeasureExpression>
    """

    MeasureExpression expression = new MeasureExpression()
    expression.load(xml)

    assert expression.sqlList[0].dialect == "oracle"
    assert expression.sqlList[0].text.trim() ==
        "(case when \"sales_fact_1997\".\"promotion_id\" = 0 then 0 else \"sales_fact_1997\".\"store_sales\" end)"

    assert expression.sqlList[1].dialect == "hsqldb"
    assert expression.sqlList[1].text.trim() ==
        "(case when \"sales_fact_1997\".\"promotion_id\" = 0 then 0 else \"sales_fact_1997\".\"store_sales\" end)"
  }
}
