package com.bytekast.commons.util

import org.codehaus.groovy.tools.xml.DomToGroovy
import org.junit.Test

/**
 * @author Rowell Belen
 */
class TempTest {

  @Test
  void testXmi() {
    def builder = javax.xml.parsers.DocumentBuilderFactory.newInstance().newDocumentBuilder()

    def xmlFile = new File(getClass().getResource("/").getPath() + 'sales_model_1.xmi').text

    def inputStream = new ByteArrayInputStream(xmlFile.bytes)
    def document = builder.parse(inputStream)
    def output = new StringWriter()
    def converter = new DomToGroovy(new PrintWriter(output))

    converter.print(document)
    println output.toString()
  }

  @Test
  void testMondrian() {
    def builder = javax.xml.parsers.DocumentBuilderFactory.newInstance().newDocumentBuilder()

    def xmlFile = new File(getClass().getResource("/").getPath() + 'FoodMart.xml').text

    def inputStream = new ByteArrayInputStream(xmlFile.bytes)
    def document = builder.parse(inputStream)
    def output = new StringWriter()
    def converter = new DomToGroovy(new PrintWriter(output))

    converter.print(document)
    println output.toString()
  }
}
