package com.bytekast.commons.util

import org.junit.Before
import org.junit.Test

/**
 * @author Rowell Belen
 */
class XmlUtilTest {

  XmlUtil xmlUtil;

  @Before
  void setup() {
    xmlUtil = new XmlUtil();
  }

  @Test
  void testFindValueByElement() {

    def xml = sampleXml();
    def result = xmlUtil.findValueByElement(xml, "title");
    assert 4 == result.size();

    result = xmlUtil.findValueByElement(xml, "nothing");
    assert 0 == result.size();

    result = xmlUtil.findValueByElement(xml, null);
    assert 0 == result.size();
  }

  @Test
  public void testFindValueByElementAndAttribute() throws Exception {
    def xml = sampleXml();
    def result = xmlUtil.findValueByElementAndAttribute(xml, "author", "id", "3");
    assert result == "Lewis Carroll";

    result = xmlUtil.findValueByElementAndAttribute(xml, "author", "blah", "3");
    assert result == null;
  }

  String sampleXml() {
    '''
    <response version-api="2.0">
      <value>
        <books>
          <book available="20" id="1">
            <title>Don Xijote</title>
            <author id="1">Manuel De Cervantes</author>
          </book>
          <book available="14" id="2">
            <title>Catcher in the Rye</title>
            <author id="2">JD Salinger</author>
          </book>
          <book available="13" id="3">
            <title>Alice in Wonderland</title>
            <author id="3">Lewis Carroll</author>
          </book>
          <book available="5" id="4">
            <title>Don Xijote</title>
            <author id="4">Manuel De Cervantes</author>
          </book>
        </books>
      </value>
    </response>
    '''
  }
}
